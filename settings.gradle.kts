pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}

dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        maven("https://maven.aliyun.com/nexus/content/groups/public/")
        maven("https://maven.aliyun.com/nexus/content/repositories/jcenter")
        maven("https://maven.aliyun.com/nexus/content/repositories/google")
        maven("https://maven.aliyun.com/nexus/content/repositories/gradle-plugin")
        maven("https://jitpack.io")
        google()
        mavenCentral()
        jcenter()
    }
}

rootProject.name = "2024AndroidTemplate"
include(":app")
include(":app-api")
include(":cpt-auth")
include(":cpt-auth-api")
include(":cpt-profile")
include(":cpt-profile-api")
include(":cs-baselib")
include(":cs-service")

//独立运行模块
include(":standalone:runalone-auth")
include(":standalone:runalone-profile")
