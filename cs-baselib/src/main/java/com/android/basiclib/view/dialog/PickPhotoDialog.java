package com.android.basiclib.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.android.basiclib.R;


/**
 * 选择相机相册对话框
 */
public class PickPhotoDialog extends Dialog implements View.OnClickListener {
    /**
     * 初始化此类，调用此构造方法。传递一个样式布局给两个参数的构造方法。由两个参数的构造方法
     * 填充布局，实现逻辑
     */
    public PickPhotoDialog(Context context) {
        this(context, R.style.quick_option_dialog);
    }

    protected PickPhotoDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    //两个参数的构造方法实现具体的逻辑
    public PickPhotoDialog(Context context, int themeResId) {
        super(context, themeResId);
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_quick_choose, null);
        view.findViewById(R.id.choose_xiangji).setOnClickListener(this);
        view.findViewById(R.id.choose_xianggce).setOnClickListener(this);
        view.findViewById(R.id.cancel).setOnClickListener(this);
        requestWindowFeature(Window.FEATURE_NO_TITLE);  //设置没有标题
        //设置触摸一下整个View.让其可取消。触摸（不是点击）对话框任意地方取消对话框
        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                dismiss();
                return true;  //消费掉此次触摸事件
            }
        });

        //View设置完成，赋值给dialog对话框
        super.setContentView(view);
    }

    /**
     * 对话框被创建调用的方法
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //设置对话框显示的位置.在底部显示
        getWindow().setGravity(Gravity.BOTTOM);
        //设置对话框的宽度，填充屏幕
        WindowManager wm = getWindow().getWindowManager();
        Display display = wm.getDefaultDisplay();
        int width = display.getWidth();
        //获取对话框的属性
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = width; //和屏幕一样宽
        getWindow().setAttributes(params);
    }

    /**
     * 点击事件
     */
    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.cancel) {
            dismiss();

        } else if (i == R.id.choose_xiangji) {
            dismiss();
            if (mListener != null) {
                mListener.chooseCamera();
            }

        } else if (i == R.id.choose_xianggce) {
            dismiss();
            if (mListener != null) {
                mListener.chooseAlbum();
            }

        }
    }


    /* =========================== 回调 =====================================**/

    private OnChooseClickListener mListener;

    public void SetOnChooseClickListener(OnChooseClickListener listener) {
        mListener = listener;
    }

    public interface OnChooseClickListener {
        void chooseCamera();

        void chooseAlbum();
    }
}
