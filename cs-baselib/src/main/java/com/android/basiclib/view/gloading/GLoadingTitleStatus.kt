package com.android.basiclib.view.gloading

class GLoadingTitleStatus {
    @JvmField
    var isShowTitle = false //默认不显示Title title的宽度为1 漏出来显示你自己的TitleBar

    @JvmField
    var isShowLoadingStr = true //Loading的文本默认展示

    @JvmField
    var isImmersive = false //知否需要支持沉浸式

    constructor()

    constructor(
        isShowTitle: Boolean = false,
        isShowBack: Boolean = true,
        titleStr: String? = null,
        isShowLoadingStr: Boolean = true,
        isImmersive: Boolean = false
    ) {
        this.isShowTitle = isShowTitle
        this.isShowLoadingStr = isShowLoadingStr
        this.isImmersive = isImmersive
    }

}