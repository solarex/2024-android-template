package com.android.basiclib.view.gloading;

import static com.android.basiclib.view.gloading.Gloading.STATUS_EMPTY_DATA;
import static com.android.basiclib.view.gloading.Gloading.STATUS_LOADING;
import static com.android.basiclib.view.gloading.Gloading.STATUS_LOAD_FAILED;
import static com.android.basiclib.view.gloading.Gloading.STATUS_LOAD_SUCCESS;
import static com.android.basiclib.view.gloading.Gloading.STATUS_NORMAL;

import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.basiclib.R;
import com.android.basiclib.view.progress.rotate.CircleLoadingView;
import com.android.basiclib.view.titlebar.EasyTitleBar;


/**
 * 这是自定义的三色圈动画
 */
@SuppressLint("ViewConstructor")
public class GloadingRoatingStatusView extends LinearLayout implements View.OnClickListener {

    public static String HIDE_LOADING_STATUS_MSG = "hide_loading_status_msg";  //是否需要展示message文本
    public static String NEED_LOADING_STATUS_MAGRIN_TITLE = "loading_status_magrin_title";  //是否需要顶部的margin
    public static String NEED_BACK_WITH_TITLE_NAME = "";
    public static String NOT_NEED_BACK_WITH_TITLE_NAME = "";

    private final TextView mTextView;
    private Runnable mRetryTask = null;
    private final ImageView mImageView;
    private final EasyTitleBar mTitle;
    private final CircleLoadingView loading_view;

    public GloadingRoatingStatusView(Context context, Runnable retryTask) {
        super(context);
        setOrientation(VERTICAL);
        setGravity(Gravity.CENTER_HORIZONTAL);
        LayoutInflater.from(context).inflate(R.layout.view_gloading_roating_status, this, true);
        mImageView = findViewById(R.id.image);
        loading_view = findViewById(R.id.clv_loading);
        mTextView = findViewById(R.id.text);
        mTitle = findViewById(R.id.title);

        if (retryTask != null) {
            this.mRetryTask = retryTask;
        }
    }

    //是否显示文本
    public void setMsgViewVisibility(boolean visible) {
        mTextView.setVisibility(visible ? VISIBLE : GONE);
    }

    //是否显示Title
    public void setTitleBarVisibility(boolean visible) {
        mTitle.setVisibility(visible ? VISIBLE : GONE);
    }

    //设置是否沉浸式
    public void setImmersive(boolean isImmersive) {
        mTitle.setHasStatusPadding(isImmersive);
    }

    public void setStatus(int status, String msg) {
        boolean show = true;  //是否展示这个布局

        OnClickListener onClickListener = null;
        int image = R.drawable.anim_gloading;
        loading_view.setVisibility(VISIBLE);
        mImageView.setVisibility(GONE);
        String str = "加载中...";

        switch (status) {
            case STATUS_LOAD_SUCCESS:
                loading_view.setVisibility(GONE);
                loading_view.stopAnimation();
                mImageView.setVisibility(GONE);
                show = false;
                onClickListener = null;
                break;

            case STATUS_NORMAL:
            case STATUS_LOADING:
                loading_view.setVisibility(VISIBLE);
                loading_view.startAnimation();
                mImageView.setVisibility(GONE);
                str = "加载中...";
                onClickListener = null;
                break;

            case STATUS_LOAD_FAILED:
                loading_view.setVisibility(GONE);
                loading_view.stopAnimation();
                mImageView.setVisibility(VISIBLE);
                //是否需要加网络状态判断
//                boolean networkConn = NetWorkUtil.isConnected(getContext());
//                if (!networkConn) {
//                    str = "NetWork Error";
//                    image = R.mipmap.page_icon_network;
//                } else {
                str = TextUtils.isEmpty(msg) ? "加载错误" : msg;
                image = R.mipmap.loading_error;
//                }
                onClickListener = this;
                break;

            case STATUS_EMPTY_DATA:
                loading_view.setVisibility(GONE);
                loading_view.stopAnimation();
                mImageView.setVisibility(VISIBLE);
                str = "没有数据";
                image = R.mipmap.loading_error;
                onClickListener = null;
                break;

            default:
                onClickListener = null;
                break;
        }

        mImageView.setImageResource(image);
        setOnClickListener(onClickListener);

        mTextView.setText(str);
        setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onClick(View v) {
        if (mRetryTask != null) {
            mRetryTask.run();
        }
    }

    public void setRetryTask(Runnable retryTask) {
        if (retryTask != null) {
            mRetryTask = retryTask;
        }

    }

}
