package com.android.basiclib.utils;

import java.text.DecimalFormat;

/**
 * 格式化数字
 */
public class NumberUtils {

    /**
     * 判断当前的字符串是否是数字
     */
    public static boolean isNumber(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 判断字符串第一位数是否是数字
     */
    public static boolean isStartWithNumber(String value) {
        if (!CheckUtil.isEmpty(value)) {
            String firstStr = value.substring(0, 1);
            return isNumber(firstStr);
        }
        return false;
    }

    /**
     * 格式化金额
     */
    public static String formatMoney(String value) {

        if (CheckUtil.checkNumberPoint(value)) {
            DecimalFormat df = new DecimalFormat("###,###,##0.00");
            value = df.format(Double.parseDouble(value));
        }

        return value;
    }

    /**
     * 格式化金额
     */
    public static String formatMoneyNoComma(String value) {

        if (CheckUtil.checkNumberPoint(value)) {
            DecimalFormat df = new DecimalFormat("########0.00");
            value = df.format(Double.parseDouble(value));
        }

        return value;
    }

    /**
     * 格式化Num，010
     */
    public static String formatNum(String value) {

        if (CheckUtil.checkNumberPoint(value)) {
            DecimalFormat df = new DecimalFormat("###,###,###");
            value = df.format(Double.parseDouble(value));
        }

        return value;
    }

    /**
     * 格式化小数点,一定带2位小数点
     */
    public static String formatPoint(String value) {

        if (CheckUtil.checkNumberPoint(value)) {
            DecimalFormat df = new DecimalFormat("###,###,##0.00");
            value = df.format(Double.parseDouble(value));
        }

        return value;
    }

    /**
     * 格式化小数点，一定带1位小数点以上,最多2位小数点
     */
    public static String formatPoint2(String value) {

        if (CheckUtil.checkNumberPoint(value)) {
            DecimalFormat df = new DecimalFormat("##0.0#");
            value = df.format(Double.parseDouble(value));
        }

        return value;
    }

    /**
     * 格式化小数点，只要带1位小数点
     */
    public static String formatPoint1(String value) {

        if (CheckUtil.checkNumberPoint(value)) {
            DecimalFormat df = new DecimalFormat("##0.0");
            value = df.format(Double.parseDouble(value));
        }

        return value;
    }


    /**
     * 格式化小数点，最多带1位小数点 如果没有小数点就不带小数点
     * 把一个数字转换为小数点格式，如果没有小数点后面如果是0就不显示
     */
    public static String formatZhengNum(String totalMoney) {
        //先格式化小数点
        totalMoney = NumberUtils.formatPoint2(totalMoney);

        //格式化小数点，如果小数点后面为0则去掉小数点
        int index = totalMoney.indexOf(".");

        String substring = totalMoney.substring(index + 1);
        try {
            int parseInt = Integer.parseInt(substring);
            if (parseInt == 0) {
                //不展示小数点
                String showTotalMoney = totalMoney.substring(0, index);
                return showTotalMoney;
            } else {
                //展示小数点
                return totalMoney;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            //展示小数点
            return totalMoney;
        }
    }

    /**
     * 格式化电话号码
     * +8618571458166这种格式中去掉区号获取到真正的电话
     */
    public static String getRealMobile(String mobile) {
        mobile = mobile.trim();

        //判断属于哪一个国家区号
        if (mobile.startsWith("+1")) {
            return mobile.substring(2);
        } else if (mobile.startsWith("+44")) {
            return mobile.substring(3);
        } else if (mobile.startsWith("+60")) {
            return mobile.substring(3);
        } else if (mobile.startsWith("+61")) {
            return mobile.substring(3);
        } else if (mobile.startsWith("+63")) {
            return mobile.substring(3);
        } else if (mobile.startsWith("+65")) {
            return mobile.substring(3);
        } else if (mobile.startsWith("+66")) {
            return mobile.substring(3);
        } else if (mobile.startsWith("+84")) {
            return mobile.substring(3);
        } else if (mobile.startsWith("+86")) {
            return mobile.substring(3);
        } else if (mobile.startsWith("+852")) {
            return mobile.substring(4);
        } else {
            return mobile;
        }

    }

    /**
     * 格式化电话号码
     * +8618571458166这种格式中去掉区号获取到区号
     */
    public static String getMobileCode(String mobile) {
        mobile = mobile.trim();

        //判断属于哪一个国家区号
        if (mobile.startsWith("1") || mobile.startsWith("+1")) {
            return "+1";
        } else if (mobile.startsWith("44") || mobile.startsWith("+44")) {
            return "+44";
        } else if (mobile.startsWith("60") || mobile.startsWith("+60")) {
            return "+60";
        } else if (mobile.startsWith("61") || mobile.startsWith("+61")) {
            return "+61";
        } else if (mobile.startsWith("63") || mobile.startsWith("+63")) {
            return "+63";
        } else if (mobile.startsWith("65") || mobile.startsWith("+65")) {
            return "+65";
        } else if (mobile.startsWith("66") || mobile.startsWith("+66")) {
            return "+66";
        } else if (mobile.startsWith("84") || mobile.startsWith("+84")) {
            return "+84";
        } else if (mobile.startsWith("86") || mobile.startsWith("+86")) {
            return "+86";
        } else if (mobile.startsWith("852") || mobile.startsWith("+852")) {
            return "+852";
        } else {
            return "";
        }

    }
}
