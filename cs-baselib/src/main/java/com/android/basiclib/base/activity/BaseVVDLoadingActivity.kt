package com.android.basiclib.base.activity

import android.os.Bundle
import android.view.LayoutInflater
import androidx.viewbinding.ViewBinding
import com.android.basiclib.base.vm.BaseViewModel
import java.lang.reflect.ParameterizedType

/**
 * 继承自 BaseVMLoadingActivity ，加入ViewBinding
 * 默认为GLoading的布局Loading加载方式
 */
abstract class BaseVVDLoadingActivity<VM : BaseViewModel, VB : ViewBinding> : BaseVMLoadingActivity<VM>() {

    private var _binding: VB? = null
    protected val mBinding: VB
        get() = requireNotNull(_binding) { "ViewBinding对象为空" }

    // 反射创建ViewBinding
    protected open fun createViewBinding() {

        try {
            val clazz: Class<*> = (this.javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[1] as Class<VB>
            val inflateMethod = clazz.getMethod("inflate", LayoutInflater::class.java)
            _binding = inflateMethod.invoke(null, layoutInflater) as VB
        } catch (e: Exception) {
            e.printStackTrace()
            throw IllegalArgumentException("无法通过反射创建ViewBinding对象")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        createViewBinding()
        super.onCreate(savedInstanceState)
    }

    override fun setContentView() {
        setContentView(mBinding.root)
    }

    override fun getLayoutIdRes(): Int = 0

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
