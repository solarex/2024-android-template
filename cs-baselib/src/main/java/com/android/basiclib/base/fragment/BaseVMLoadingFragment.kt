package com.android.basiclib.base.fragment

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.android.basiclib.utils.NetWorkUtil
import com.android.basiclib.view.dialog.LoadingDialogManager
import com.android.basiclib.view.gloading.Gloading
import com.android.basiclib.base.vm.BaseViewModel
import com.android.basiclib.bean.LoadAction
import com.android.basiclib.ext.getVMCls

/**
 * 加入ViewModel与LoadState
 * 默认为Loading布局的加载方式
 */
abstract class BaseVMLoadingFragment<VM : BaseViewModel> : AbsFragment() {

    protected lateinit var mViewModel: VM

    protected lateinit var mGLoadingHolder: Gloading.Holder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        startObserve()

        init(savedInstanceState)
    }

    protected open fun initViewModel() {
        mViewModel = createViewModel()
    }

    protected open fun startObserve() {
        //观察网络数据状态
        mViewModel.getActionLiveData().observe(viewLifecycleOwner, stateObserver)
    }

    override fun transformRootView(view: View): View {
        mGLoadingHolder = generateGLoading(view)
        return mGLoadingHolder.wrapper
    }

    //如果要替换GLoading，重写次方法
    protected open fun generateGLoading(view: View): Gloading.Holder {
        return Gloading.getDefault().wrap(view).withRetry {
            onGoadingRetry()
        }
    }

    protected open fun onGoadingRetry() {
    }

    //使用这个方法简化ViewModewl的Hilt依赖注入获取
    protected inline fun <reified VM : BaseViewModel> getViewModel(): VM {
        val viewModel: VM by viewModels()
        return viewModel
    }

    //反射获取ViewModel实例
    protected open fun createViewModel(): VM {
        return ViewModelProvider(this).get(getVMCls(this))
    }

    override fun setContentView(container: ViewGroup?): View {
        return layoutInflater.inflate(getLayoutIdRes(), container, false)
    }

    /**
     * 获取layout的id，具体由子类实现
     */
    abstract fun getLayoutIdRes(): Int
    abstract fun init(savedInstanceState: Bundle?)

    override fun onNetworkConnectionChanged(isConnected: Boolean, networkType: NetWorkUtil.NetworkType?) {
    }

    // ================== 网络状态的监听 ======================

    private var stateObserver: Observer<LoadAction> = Observer { loadAction ->
        when (loadAction.action) {
            LoadAction.STATE_NORMAL -> showStateNormal()
            LoadAction.STATE_ERROR -> showStateError(loadAction.message)
            LoadAction.STATE_SUCCESS -> showStateSuccess()
            LoadAction.STATE_LOADING -> showStateLoading()
            LoadAction.STATE_NO_DATA -> showStateNoData()
            LoadAction.STATE_PROGRESS -> showStateProgress()
            LoadAction.STATE_HIDE_PROGRESS -> hideStateProgress()
        }
    }

    protected open fun showStateNormal() {}

    protected open fun showStateLoading() {
        mGLoadingHolder.showLoading()
    }

    protected open fun showStateSuccess() {
        mGLoadingHolder.showLoadSuccess()
    }

    protected open fun showStateError(message: String?) {
        mGLoadingHolder.showLoadFailed(message)
    }

    protected open fun showStateNoData() {
        mGLoadingHolder.showEmpty()
    }

    protected fun showStateProgress() {
        LoadingDialogManager.get().showLoading(mActivity)
    }

    protected fun hideStateProgress() {
        LoadingDialogManager.get().dismissLoading()
    }

}