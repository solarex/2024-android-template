package com.newki.profile_api.http

import com.android.basiclib.base.BaseRetrofitClient
import com.android.cs_service.Constants

object ProfileRetrofit : BaseRetrofitClient() {

    //默认的ApiService
    val apiService by lazy { getService(ProfileApiService::class.java, Constants.BASE_URL) }

}