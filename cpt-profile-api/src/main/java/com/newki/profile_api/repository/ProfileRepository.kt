package com.newki.profile_api.repository

import com.android.basiclib.base.vm.BaseRepository
import com.android.basiclib.bean.OkResult
import com.android.basiclib.engine.network.httpRequest
import com.newki.profile_api.entity.Banner
import com.newki.profile_api.entity.TopArticleBean
import com.newki.profile_api.http.ProfileRetrofit
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class ProfileRepository @Inject constructor() : BaseRepository() {

    /**
     * 使用扩展方法，自己的引擎类请求网络
     */
    suspend inline fun fetchBanner(): OkResult<List<Banner>> {

        return httpRequest {
            ProfileRetrofit.apiService.fetchBanner()
        }

    }

    /**
     * 使用基类方法嵌套请求网络
     */
    suspend inline fun fetchTopArticle(): OkResult<List<TopArticleBean>> {

        return handleErrorApiCall(call = {
            handleApiErrorResponse(
                ProfileRetrofit.apiService.fetchTopArticle()
            )
        })
    }


}