package com.runalone.auth.ui

import android.os.Bundle
import com.android.basiclib.base.activity.BaseVVDActivity
import com.android.basiclib.ext.click
import com.newki.auth.ui.AuthLoginActivity
import com.runalone.auth.databinding.ActivityRunaloneBinding
import com.runalone.auth.mvvm.RunaloneViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * 独立运行模块入口
 */
@AndroidEntryPoint
class RunaloneActivity : BaseVVDActivity<RunaloneViewModel, ActivityRunaloneBinding>() {

    override fun init(savedInstanceState: Bundle?) {

        mBinding.btnLogin.click {
            AuthLoginActivity.startInstance()
            finish()
        }

    }

}