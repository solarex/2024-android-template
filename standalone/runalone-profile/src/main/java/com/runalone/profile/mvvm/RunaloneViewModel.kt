package com.runalone.profile.mvvm

import com.android.basiclib.base.vm.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RunaloneViewModel @Inject constructor(
) : BaseViewModel() {

}