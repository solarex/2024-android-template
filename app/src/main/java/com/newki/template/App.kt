package com.newki.template

import com.android.basiclib.base.BaseApplication
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App :BaseApplication(){
    override fun onCreate() {
        super.onCreate()
    }

}