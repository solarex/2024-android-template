package com.newki.template.di

import android.app.Application
import com.google.gson.Gson
import com.hjq.gson.factory.GsonFactory
import com.newki.template.App
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * 全局的DI注入
 */
@Module
@InstallIn(SingletonComponent::class)
class ApplicationDIModule {

    @Provides
    fun provideMyApplication(application: Application): App {
        return application as App
    }

}